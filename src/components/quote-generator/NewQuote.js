import React from 'react';
import ReactDOM from 'react-dom';
import { newQuoteStyle } from '../styles/QuoteButtonStyle';
import { GenerateQuote } from 'GenerateQuote';


export class NewQuote extends React.Component {

   


    render() {
        return (    
            <div>
                <h1 style={newQuoteStyle} onClick={<GenerateQuote />}>Generate Quote!</h1>
            </div>
        )
    }
}

ReactDOM.render(<NewQuote />, document.getElementById('app'));